package smoslt.data;

import org.btrg.uti.StringUtils_;
import org.btrg.utils.random.RandomParagraph;
import org.btrg.utils.random.RandomProvider;
import org.btrg.utils.random.RandomSentence;
import org.btrg.utils.random.RandomWord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import smoslt.data.domain.Customer;
import smoslt.data.domain.ProjectPhase;
import smoslt.data.domain.ScoreKey;
import smoslt.data.domain.ScoreMunger;
import smoslt.data.domain.SideEffect;
import smoslt.data.domain.Task;
import smoslt.data.domain.TeamInventory;
import smoslt.data.domain.Technology;
import smoslt.data.domain.TechnologyGroup;
import smoslt.data.domain.Worker;
import smoslt.data.repository.CustomerRepository;
import smoslt.data.repository.ProjectPhaseRepository;
import smoslt.data.repository.ScoreKeyRepository;
import smoslt.data.repository.ScoreMungerRepository;
import smoslt.data.repository.SideEffectRepository;
import smoslt.data.repository.TaskRepository;
import smoslt.data.repository.TeamInventoryRepository;
import smoslt.data.repository.TechnologyGroupRepository;
import smoslt.data.repository.TechnologyRepository;
import smoslt.data.repository.WorkerRepository;

@SpringBootApplication
public class Application {
	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public CommandLineRunner demo(CustomerRepository customerRepository, ProjectPhaseRepository projectPhaseRepository,
			ScoreKeyRepository scoreKeyRepository, ScoreMungerRepository scoreMungerRepository,
			SideEffectRepository sideEffectRepository, TaskRepository taskRepository,
			TeamInventoryRepository teamInventoryRepository, TechnologyGroupRepository technologyGroupRepository,
			TechnologyRepository technologyRepository, WorkerRepository workerRepository) {
		return (args) -> {
			log.info("-------------------------------START POPULATING DATABASE");
			Iterable<Customer> customers = customerRepository.findAll();
			long customerCount = customerRepository.count();
			long projectPhaseCount = projectPhaseRepository.count();
			long scoreKeyCount = scoreKeyRepository.count();
			long scoreMungerCount = scoreMungerRepository.count();
			long sideEffectCount = sideEffectRepository.count();
			long taskCount = taskRepository.count();
			long teamInventoryCount = teamInventoryRepository.count();
			long technologyGroupCount = technologyGroupRepository.count();
			long technologyCount = technologyRepository.count();
			long workerCount = workerRepository.count();
			for (int i = 0; i + customerCount < 100; customerCount++) {
				Customer customer = new Customer(RandomProvider.randomName(), StringUtils_.randomString(3, false),
						"STANDARD");
				customerRepository.save(customer);
			}
			log.info("-------------------------------DONE POPULATING customer WITH " + customerCount);
			for (int i = 0; i + projectPhaseCount < 14; projectPhaseCount++) {
				ProjectPhase projectPhase = new ProjectPhase(RandomWord.getWord(), RandomSentence.getSentence(),
						StringUtils_.randomString(4, false).toUpperCase());
				projectPhaseRepository.save(projectPhase);
			}
			log.info("-------------------------------DONE POPULATING projectPhase WITH " + projectPhaseCount);
			for (int i = 0; i + scoreKeyCount < 25; scoreKeyCount++) {
				ScoreKey scoreKey = new ScoreKey(RandomWord.getWord(), RandomSentence.getSentence(),
						StringUtils_.randomString(6, false).toUpperCase());
				scoreKeyRepository.save(scoreKey);
			}
			log.info("-------------------------------DONE POPULATING scoreKey WITH " + scoreKeyCount);
			for (int i = 0; i + scoreMungerCount < 25; scoreMungerCount++) {
				ScoreMunger scoreMunger = new ScoreMunger(
						StringUtils_.upLow(RandomWord.getWord()) + " " + StringUtils_.upLow(RandomWord.getWord()), RandomSentence.getSentence(),
						StringUtils_.randomString(3, false).toUpperCase());
				scoreMungerRepository.save(scoreMunger);
			}
			log.info("-------------------------------DONE POPULATING scoreMunger WITH " + scoreMungerCount);
			for (int i = 0; i + sideEffectCount < 140; sideEffectCount++) {
				SideEffect sideEffect = new SideEffect(RandomWord.getWord() + " " + RandomWord.getWord(),
						RandomSentence.getSentence(), StringUtils_.randomString(3, false).toUpperCase(), false, true,
						true, false, RandomWord.getWord(), RandomWord.getWord());
				sideEffectRepository.save(sideEffect);
			}
			log.info("-------------------------------DONE POPULATING sideEffect WITH " + sideEffectCount);
			for (int i = 0; i + taskCount < 300; taskCount++) {
				int startDay = (int) (Math.random() * 150);
				int durationHours = (int) (Math.random() * 150);
				Task task = new Task(StringUtils_.upLow(RandomWord.getWord()) + " " + RandomWord.getWord(),
						RandomSentence.getSentence(), StringUtils_.randomString(3, false).toUpperCase(), startDay,
						durationHours, RandomWord.getWord());
				taskRepository.save(task);
			}
			log.info("-------------------------------DONE POPULATING task WITH " + taskCount);
			for (int i = 0; i + teamInventoryCount < 50; teamInventoryCount++) {
				int choice = (int) (Math.random() * 16);
				int topRange = (int) (Math.random() * 150);
				int bottomRange = (int) (Math.random() * 10);
				String text = RandomParagraph.getParagraph();
				if(text.length()>250){
					text = text.substring(0,250)+ ".";
				}
				TeamInventory teamInventory = new TeamInventory(RandomWord.getWord().toUpperCase(),
						StringUtils_.upLow(RandomWord.getWord()), RandomSentence.getSentence(), choice,
						RandomWord.getWord(), topRange, bottomRange, text);
				teamInventoryRepository.save(teamInventory);
			}
			log.info("-------------------------------DONE POPULATING teamInventory WITH " + teamInventoryCount);
			for (int i = 0; i + technologyGroupCount < 40; technologyGroupCount++) {
				TechnologyGroup technologyGroup = new TechnologyGroup(RandomWord.getWord().toUpperCase(),
						RandomSentence.getSentence(), StringUtils_.randomString(4, false).toUpperCase());
				technologyGroupRepository.save(technologyGroup);
			}
			log.info("-------------------------------DONE POPULATING technologyGroup WITH " + technologyGroupCount);
			for (int i = 0; i + technologyCount < 100; technologyCount++) {
				Technology technology = new Technology(
						StringUtils_.upLow(RandomWord.getWord()) + " " + RandomWord.getWord(),
						RandomSentence.getSentence(), StringUtils_.randomString(3, false).toUpperCase());
				technologyRepository.save(technology);
			}
			log.info("-------------------------------DONE POPULATING technology WITH " + technologyCount);
			for (int i = 0; i + workerCount < 100; workerCount++) {
				String name = org.btrg.utils.random.RandomProvider.randomName();
				Worker worker = new Worker(name.substring(0, name.indexOf(" ")),
						name.substring(name.indexOf(" "), name.length() - 1), RandomWord.getWord(), "Chicago",
						StringUtils_.randomNumberString(10),
						StringUtils_.randomString(9, true).toLowerCase() + "@" + StringUtils_.randomString(6, false).toLowerCase() + ".com");
				workerRepository.save(worker);
			}
			log.info("-------------------------------DONE POPULATING worker WITH" + workerCount);
			log.info("-------------------------------DONE POPULATING DATABASE");
		};
	}
}

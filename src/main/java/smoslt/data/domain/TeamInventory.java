package smoslt.data.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class TeamInventory {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String category;
	private String name;
	private String descrip;
	private int choice;
	private String value;
	private int bottomRange;
	private int topRange;
	private String text;

	public TeamInventory() {

	}

	public TeamInventory(String category, String name, String description, int choice, String value, int bottomRange,
			int topRange, String text) {
		this.category = category;
		this.name = name;
		this.descrip = description;
		this.choice = choice;
		this.value = value;
		this.bottomRange = bottomRange;
		this.topRange = topRange;
		this.text = text;
	}

}

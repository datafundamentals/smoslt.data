package smoslt.data.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Worker {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String firstName;
	private String lastName;
	private String primarySkill;
	private String city;
	private String phone;
	private String email;

	public Worker() {

	}

	public Worker(String firstName, String lastName, String primarySkill, String city, String phone, String email) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.primarySkill = primarySkill;
		this.city = city;
		this.phone = phone;
		this.email = email;
	}

}
package smoslt.data.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Task {
	private static final long serialVersionUID = 683552385554970295L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private String descrip;
	private String abrv;
	private long authorId;
	private int startDay;
	private int durationHours;
	private String resourceSpecifications;

	public Task() {

	}

	public Task(String name, String descrip, String abrv, int startDay, int durationHours,
			String resourceSpecifications) {
		this.name = name;
		this.descrip = descrip;
		this.abrv = abrv;
		this.startDay = startDay;
		this.durationHours = durationHours;
		this.resourceSpecifications = resourceSpecifications;
	}
}

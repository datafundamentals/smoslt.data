package smoslt.data.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class ScoreMunger {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private String descrip;
	private String abrv;
	private long authorId;

	public ScoreMunger() {

	}

	public ScoreMunger(String name, String descrip, String abrv) {
		this.name = name;
		this.descrip = descrip;
		this.abrv = abrv;
	}
}

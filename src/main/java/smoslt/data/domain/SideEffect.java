package smoslt.data.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class SideEffect {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private String descrip;
	private String abrv;
	private boolean manadatory;
	private long authorId;
	private boolean isNameCompliant;
	private boolean isOrGroupName;
	private boolean isOrGroupMember;
	private boolean isOrGroupTitle;
	private String effectKey;
	private String groupKey;
	private boolean isSaturationScored;

	public SideEffect() {
	}

	public SideEffect(String name, String descrip, String abrv, boolean isNameCompliant, boolean isOrGroupName,
			boolean isOrGroupMember, boolean isOrGroupTitle, String effectKey, String groupKey) {
		super();
		this.name = name;
		this.descrip = descrip;
		this.abrv = abrv;
		this.isNameCompliant = isNameCompliant;
		this.isOrGroupName = isOrGroupName;
		this.isOrGroupMember = isOrGroupMember;
		this.isOrGroupTitle = isOrGroupTitle;
		this.effectKey = effectKey;
		this.groupKey = groupKey;
	}
}

package smoslt.data.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import smoslt.data.domain.Technology;

@RepositoryRestResource(collectionResourceRel = "technology", path = "technology")
public interface TechnologyRepository extends PagingAndSortingRepository<Technology, Long> {

	List<Technology> findByName(@Param("name") String name);

}

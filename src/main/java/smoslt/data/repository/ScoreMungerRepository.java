package smoslt.data.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import smoslt.data.domain.ScoreMunger;

@RepositoryRestResource(collectionResourceRel = "scoremunger", path = "scoremunger")
public interface ScoreMungerRepository extends PagingAndSortingRepository<ScoreMunger, Long> {

	List<ScoreMunger> findByName(@Param("name") String name);

}

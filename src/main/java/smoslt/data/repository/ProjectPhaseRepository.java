package smoslt.data.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import smoslt.data.domain.ProjectPhase;

@RepositoryRestResource(collectionResourceRel = "projectphase", path = "projectphase")
public interface ProjectPhaseRepository extends PagingAndSortingRepository<ProjectPhase, Long> {

	List<ProjectPhase> findByName(@Param("name") String name);

}

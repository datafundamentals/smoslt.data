package smoslt.data.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import smoslt.data.domain.TechnologyGroup;

@RepositoryRestResource(collectionResourceRel = "technologygroup", path = "technologygroup")
public interface TechnologyGroupRepository extends PagingAndSortingRepository<TechnologyGroup, Long> {

	List<TechnologyGroup> findByName(@Param("name") String name);

}

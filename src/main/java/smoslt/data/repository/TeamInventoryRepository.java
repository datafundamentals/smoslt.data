package smoslt.data.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import smoslt.data.domain.TeamInventory;

@RepositoryRestResource(collectionResourceRel = "teaminventory", path = "teaminventory")
public interface TeamInventoryRepository extends PagingAndSortingRepository<TeamInventory, Long> {

	List<TeamInventory> findByName(@Param("name") String name);

}

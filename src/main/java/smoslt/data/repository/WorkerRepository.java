package smoslt.data.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import smoslt.data.domain.Worker;

@RepositoryRestResource(collectionResourceRel = "worker", path = "worker")
public interface WorkerRepository extends PagingAndSortingRepository<Worker, Long> {

	List<Worker> findByLastName(@Param("name") String name);

}

package smoslt.data.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import smoslt.data.domain.ScoreKey;

//optional just allows me to set scorekey name and link instead of setting a defaults for me
@RepositoryRestResource(collectionResourceRel = "scorekey", path = "scorekey")
public interface ScoreKeyRepository extends PagingAndSortingRepository<ScoreKey, Long> {

	List<ScoreKey> findByName(@Param("name") String name);

}

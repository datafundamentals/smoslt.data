package smoslt.data.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import smoslt.data.domain.SideEffect;

@RepositoryRestResource(collectionResourceRel = "sideeffect", path = "sideeffect")
public interface SideEffectRepository extends PagingAndSortingRepository<SideEffect, Long> {

	List<SideEffect> findByName(@Param("name") String name);

}
